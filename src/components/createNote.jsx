import React ,{ useState } from "react";


function CreateNote( {passNote}){

    const [note,setNote]=useState({title:"",content:""});

    function inputEvent(event){
        const {name,value}=event.target;
        // console.log(event.target.name);

        setNote((prev)=>{
            return {
                ...prev,
                [name]:value,
            }
        })
    }

    function addEvent(event){
        event.preventDefault();
        passNote(note);

        setNote({
            title:"",
            content:"",
        })
    }
    return (
        <>
            <div className="center">
                <div className="note">
                    <form className="form">
                        <input type="text" 
                            placeholder="Title"
                            name="title"
                            value={note.title}
                            onChange={inputEvent}
                            // autoComplete="off"
                        />
                        <textarea rows="" 
                            column="" 
                            placeholder="Write a note" 
                            name="content"
                            value={note.content}
                            onChange={inputEvent}
                        ></textarea>
                        <button className="btn" onClick={addEvent}>+</button>
                    </form>
                </div>
            </div>
        </>
    )
}

export default CreateNote;