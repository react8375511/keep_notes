import React from "react";

const Note = (props) => {

    function deleteNote(){
        props.deleteItem(props.id);
    }
    return (
        <>
            <div className="notes">
                <h1> {props.title} </h1>
                
                <p> {props.content}</p>

                <button className="btn" onClick={deleteNote}>X</button>
            </div>
        </>
    );
};

export default Note;