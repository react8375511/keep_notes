import React ,{ useState } from 'react';
import Header from "./components/header"
import CreateNote from './components/createNote';
import Note from './components/Note';
import './index.css';

function App() {
  const [addItem,setAddItem]=useState([]);

  function addNode(note){
    setAddItem((prev)=>{
      return [...prev,note];
    })
  }

  function onDelete(id){

    setAddItem((pre)=>pre.filter((data,index)=>{
      return index!==id;
    })
    );
  }
  return (
    <>
      <Header/>
      <CreateNote passNote={addNode}/>

      <div className='notes_center'>

        {addItem.map((item,index)=>{
          
          return <Note key={index} id={index} title={item.title} content={item.content} deleteItem={onDelete}/>

        })}

      </div>
    </>
  )
}

export default App;
